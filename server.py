#!/usr/bin/env python3
from flask import Flask, jsonify, request
from crawler import get_data
from datetime import datetime
from datetime import date
import os
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route("/")
def json_api():
    # Then only update once a day
    codigo = request.args.get('codigo')
    # codigo = '0000044-37.1998.8.18.0026'
    if codigo is not None:
        data = dict(get_data(codigo))
        return jsonify(data)
        pass
    else:
        return jsonify({ 'error': True })
        pass

port = int(os.environ.get("PORT", 5000))
app.run(debug=True,host='0.0.0.0', port=port)
