#!/usr/bin/env python3

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import time
import sys
import platform
import os
import calendar
import re
import urllib.request
import urllib.parse
import http.cookiejar

from collections import OrderedDict

driver = None
#import urllib.request


def get_data(codigo, *args, **kwargs):
    options = Options()
    prefs = {"profile.managed_default_content_settings.images": 2}
    options.add_argument("--disable-notifications")
    options.add_argument("--disable-infobars")
    options.add_argument("--mute-audio")
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-gpu')
    options.add_experimental_option("prefs", prefs)

    driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=options)
    driver.get("http://siscadejus.jmcsistemas.com/sc5/abrir.php?codigo="+codigo)
    time.sleep(7)

    linha = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]')
    numero_acervo = driver.find_element(By.XPATH, '//*[@id="container"]/div[2]/div[2]/table/tbody/tr[1]/td')
    abertura = driver.find_element(By.XPATH, '//*[@id="container"]/div[2]/div[2]/table/tbody/tr[2]/td')
    comarca = driver.find_element(By.XPATH, '//*[@id="container"]/div[2]/div[2]/table/tbody/tr[3]/td')
    assistencia_jud = driver.find_element(By.XPATH, '//*[@id="container"]/div[2]/div[2]/table/tbody/tr[4]/td')
    natureza = driver.find_element(By.XPATH, '//*[@id="container"]/div[2]/div[2]/table/tbody/tr[5]/td')
    classe = driver.find_element(By.XPATH, '//*[@id="container"]/div[2]/div[2]/table/tbody/tr[6]/td')
    assuntos = driver.find_element(By.XPATH, '//*[@id="container"]/div[2]/div[2]/table/tbody/tr[7]/td')
    observacao = driver.find_element(By.XPATH, '//*[@id="container"]/div[2]/div[2]/table/tbody/tr[8]/td')
    valor = driver.find_element(By.XPATH, '//*[@id="container"]/div[2]/div[2]/table/tbody/tr[9]/td')
    status = driver.find_element(By.XPATH, '//*[@id="container"]/div[2]/div[2]/table/tbody/tr[10]/td')

    detalhes = {
        'linha': linha.text,
        'numero_acervo': numero_acervo.text,
        'abertura': abertura.text,
        'comarca': comarca.text,
        'assistencia_jud': assistencia_jud.text,
        'natureza': natureza.text,
        'classe': classe.text,
        'assuntos': assuntos.text,
        'observacao': observacao.text,
        'valor': valor.text,
        'status': status.text
    }
    driver.quit()
    print(detalhes)
    return detalhes
